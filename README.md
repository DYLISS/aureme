# AuReMe

### Context
Aite,M. Chevallier,M. Frioux,C. Trottier,C et al. (2018) [Traceability, reproducibility and wiki-exploration for “à-la-carte” reconstructions of genome-scale metabolic models](https://doi.org/10.1371/journal.pcbi.1006146) PLOS Comput. Biol., 14, e1006146.

Please notice that by downloading AuReMe, the user accepts the associated user licence, as detailed in the provisions described on the AuReMe website, and undertakes to abide by it in full. ([aureme.genouest.org](http://aureme.genouest.org))

AuReMe is distributed in the hope that it will be useful, for research purposes only and strictly academic.

Includes BioCycTM pathway/genome databases under licence from SRI International. This product includes software developed by and/or derived from the SEED project.

### Description 
AuReMe enables the reconstruction of metabolic networks from different sources based on sequence annotation, orthology, gap-filling and manual curation. The metabolic network is exported as a local wiki allowing to trace back all the steps and sources of the reconstruction. It is highly relevant for the study of non-model organisms, or the comparison of metabolic networks for different strains or a single organism.

Five modules are composing AuReMe: 1) The Model-management PADmet module allows manipulating and traceing all metabolic data via a local database. 2) The meneco python package allows the gaps of a metabolic network to be filled by using a topological approach that implements a logical programming approach to solve a combinatorial problem 3) The shogen python package allows genome and metabolic network to be aligned in order to identify genome units which contain a large density of genes coding for enzymes, it also implements a logical programming approach. 4) The manual curation assistance PADmet module allows the reported metabolic networks and their metadata to be curated. 5) The Wiki-export PADmet module enables the export of the metabolic network and its functional genomic unit as a local wiki platform allowing a user-friendly investigation. 

### Installation
```sh
docker pull registry.gitlab.inria.fr/dyliss/aureme/aureme-img:latest
```

### Manual
You can find a detailled documentation [here](https://aureme.readthedocs.io/en/latest/)

### Support
You can send an email at gem-aureme_AT_inria.fr
